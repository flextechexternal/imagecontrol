//
//  CustomTextView.swift
//  ImageControl
//
//  Created by Than Htet Aung on 7/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class ScrollableLabel: UIView, UITextViewDelegate {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    var view:UIView!
    @IBOutlet var textView: UITextView!{
        didSet{
            textView.delegate = self
        }
    }
    
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat = 0{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var topLeftRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var topRightRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var bottomLeftRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var bottomRightRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var allRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    func updateUI(){
        self.layer.borderColor = self.borderColor.cgColor;
        self.layer.borderWidth = self.borderWidth;
        if cornerRadius > 0{
            if allRadius{
                self.layer.cornerRadius = cornerRadius;
            }
            else{
                let maskLayer = CAShapeLayer();
                var corners:UIRectCorner = [];
                if(self.topLeftRadius){
                    corners.insert(UIRectCorner.topLeft);
                }
                
                if(self.topRightRadius){
                    corners.insert(UIRectCorner.topRight);
                }
                
                if(self.bottomLeftRadius){
                    corners.insert(UIRectCorner.bottomLeft);
                }
                
                if(self.bottomRightRadius){
                    corners.insert(UIRectCorner.bottomRight);
                }
                
                
                maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
                layer.mask = maskLayer
            }
            
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        updateUI();
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight];
        addSubview(view)
        label.font = UIFont.systemFont(ofSize: 50)
    }
    
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self));
        let nib = UINib(nibName: "ScrollableLabel", bundle: bundle);
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    

    
    var text:String?{
        didSet{
            label.text = text
            label.sizeToFit()
            scrollView.contentSize = label.frame.size
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
//        text = textView.text
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        text = textView.text
        scrollTo(position: textView.selectedRange.location)
    }
    
    func scrollTo(position:Int) -> Void {
        let text = self.label.text!
        guard text.characters.count >= position else{
            return
        }
    
        let label = UILabel()
        label.font = self.label.font
        let index = text.index(text.startIndex, offsetBy: position)
        label.text = text.substring(to: index)
        let size = label.sizeThatFits(label.frame.size)
    
        scrollView.scrollRectToVisible(CGRect(x:0, y:0, width: size.width, height: size.height), animated: true)
        
    }
    
}
