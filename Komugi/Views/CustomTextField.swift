//
//  CustomTextView.swift
//  ImageControl
//
//  Created by Than Htet Aung on 7/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomTextField: UITextField {
    @IBInspectable
    var borderColor: UIColor = UIColor.clear{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat = 0{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var topLeftRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var topRightRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var bottomLeftRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var bottomRightRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    @IBInspectable
    var allRadius: Bool = false{
        didSet{
            updateUI();
        }
    }
    
    func updateUI(){
        self.layer.borderColor = self.borderColor.cgColor;
        self.layer.borderWidth = self.borderWidth;
        if cornerRadius > 0{
            if allRadius{
                self.layer.cornerRadius = cornerRadius;
            }
            else{
                let maskLayer = CAShapeLayer();
                var corners:UIRectCorner = [];
                if(self.topLeftRadius){
                    corners.insert(UIRectCorner.topLeft);
                }
                
                if(self.topRightRadius){
                    corners.insert(UIRectCorner.topRight);
                }
                
                if(self.bottomLeftRadius){
                    corners.insert(UIRectCorner.bottomLeft);
                }
                
                if(self.bottomRightRadius){
                    corners.insert(UIRectCorner.bottomRight);
                }
                
                
                maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
                layer.mask = maskLayer
            }
            
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        updateUI();
    }
}
