//
//  ImageViewController.swift
//  ImageControl
//
//  Created by zarniaung on 2/28/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import UIKit
import GPUImage2

class ImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var lblBlurInPixel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    let imagePicker = UIImagePickerController();
    var pickedImage: UIImage?
    var originalImage: UIImage?
    
//    var threshold:AdaptiveThreshold!
    var threshold:LuminanceThreshold!
    var pictureInput:PictureInput!
    var pictureOutput:PictureOutput!

    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var drop: DropMenuButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.allowsEditing = false;
        imagePicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self;
        drop.isHidden = true
        
        didChangedSegment(segmentedControl)
        drop.initMenu(["Item 1", "Item 2", "Item 3", "Item 4"], actions: [({ () -> (Void) in
        }), ({ () -> (Void) in
        }), ({ () -> (Void) in
        }), ({ () -> (Void) in
        })])
        // Do any additional setup after loading the view.
    }

    
    @IBAction func didChangedSegment(_ sender: UISegmentedControl) {
        if segmentedControl.selectedSegmentIndex == 0 {
            slider.isHidden = false
//            drop.isHidden = true
            drop.table.isHidden = true
            lblBlurInPixel.isHidden = false
            banarize()
        }else{
            slider.isHidden = true
//            drop.isHidden = false
            drop.table.isHidden = false
            lblBlurInPixel.isHidden = true
            errorDiffuse()
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.originalImage = pickedImage
            self.pickedImage = self.grayImage(image:pickedImage)
            if segmentedControl.selectedSegmentIndex == 0{
                self.imageView.image = pickedImage
                
                DispatchQueue.main.async {
                     self.banarize()
                }
            }
            else{
                errorDiffuse()
            }
           
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func errorDiffuse(){
        if let _ = self.originalImage{
            let image = CVWrapper.errorDiffuseImage(withOpenCV: self.originalImage!)
            self.imageView.image = image
        }
    }
    
    func banarize(){
        if let _ = self.pickedImage{
            pictureOutput = PictureOutput()
    //        threshold = AdaptiveThreshold()
    //        threshold.blurRadiusInPixels = slider.value
            threshold = LuminanceThreshold()
            threshold.threshold = self.convertSliderValue(value: slider.value)
            threshold.removeAllTargets()
            
            threshold.addTarget(pictureOutput)
            pictureInput = PictureInput(image: pickedImage!)
            pictureInput.addTarget(threshold)
            pictureOutput.imageAvailableCallback = {(img) in
                DispatchQueue.main.async {
                    self.imageView.image = img
                }
            }
            pictureInput.processImage(synchronously: false)
            
            
    //        imageView.image = pickedImage?.filterWithOperation(threshold)
        }
      
    }
    
    var flag = false
    
    func convertSliderValue(value:Float) -> Float{
        return  value/255
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        
        lblBlurInPixel.text = "\(Int(sender.value))"
        if self.pictureInput != nil{
//            self.threshold.blurRadiusInPixels = sender.value
            self.threshold.threshold = self.convertSliderValue(value: sender.value)
        }
        guard !flag else{
            return
        }
        flag = true
        let concurrentQueue = DispatchQueue(label: "background", attributes: .concurrent)
        concurrentQueue.asyncAfter(deadline: .now() + 0.15) {
            if self.pictureInput != nil{
                print(sender.value)
//                self.pictureInput.removeAllTargets()
                self.pictureInput.addTarget(self.threshold)
//                self.threshold.blurRadiusInPixels = sender.value
                self.threshold.threshold = self.convertSliderValue(value: sender.value)
                self.flag = false
                self.pictureOutput.imageAvailableCallback = {(img) in
                    DispatchQueue.main.async {
                        self.imageView.image = img
                    }
                }
                self.pictureInput.processImage(synchronously: false)
            }
//            let img = self.pickedImage?.filterWithOperation(self.threshold)
//            DispatchQueue.main.async {
//                self.imageView.image = img
//            }
        }
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressImage(_ sender: UITapGestureRecognizer) {
        self.present(self.imagePicker, animated: true, completion: nil)
        
    }
    
    func grayImage(image:UIImage) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(image.size, false, 1.0);
        let imageRect = CGRect(x:0, y:0, width:image.size.width, height:image.size.height);
        
        // Draw the image with the luminosity blend mode.
        // On top of a white background, this will give a black and white image.
        image.draw(in: imageRect, blendMode: .luminosity, alpha: 1.0)
        
        // Get the resulting image.
        let outputImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return outputImage!
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
