//
//  QrBarViewController.swift
//  ImageControl
//
//  Created by zarniaung on 2/28/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import UIKit
import ZXingObjC

enum BarcodeFormat{
    case CODE39
    case CODE128
    case JAN13
    case ITF
    case GS1_128
    case QR
}

class QrBarViewController: UIViewController,UIAlertViewDelegate, UITextViewDelegate {

    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: CustomTextView!
    @IBOutlet weak var dropBarCode: UIButton!
    
    var selectedType:BarcodeFormat = .CODE39
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func didPressType(_ sender: UIButton) {
        let controller = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
        
        let code39 = UIAlertAction(title: "CODE39", style: .default, handler: { (_) in
            self.dropBarCode.setTitle("CODE39", for: .normal)
            self.generateBarCodeOrQR(format: .CODE39)
        })
        controller.addAction(code39)
        
        
        let code128 = UIAlertAction(title: "CODE128", style: .default, handler: { (_) in
            self.dropBarCode.setTitle("CODE128", for: .normal)
            self.generateBarCodeOrQR(format: .CODE128)
        })
        controller.addAction(code128)

        let jan13 = UIAlertAction(title: "JAN13", style: .default, handler: { (_) in
            self.dropBarCode.setTitle("JAN13", for: .normal)
            self.generateBarCodeOrQR(format: .JAN13)
        })
        controller.addAction(jan13)

        
        let itf = UIAlertAction(title: "ITF", style: .default, handler: { (_) in
            self.dropBarCode.setTitle("ITF", for: .normal)
            self.generateBarCodeOrQR(format: .ITF)
        })
        controller.addAction(itf)

        let qr = UIAlertAction(title: "QR", style: .default, handler: { (_) in
            self.dropBarCode.setTitle("QR", for: .normal)
            self.generateBarCodeOrQR(format: .QR)
        })
        controller.addAction(qr)
        
        let ean128 = UIAlertAction(title: "GS1-128", style: .default, handler: { (_) in
            self.dropBarCode.setTitle("GS1-128", for: .normal)
            self.generateBarCodeOrQR(format: .GS1_128)
        })
        controller.addAction(ean128)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
            
        })
        controller.addAction(cancel)
        
        
        
        present(controller, animated: true, completion: nil)
        
    }
    
    
    
    func generateBarCodeOrQR(format:BarcodeFormat){
        selectedType = format
        self.lblError.isHidden = true
        let text = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if text.isEmpty{
            self.imageView.image = nil
            return
        }
        if format == .GS1_128{
            SwiftTryCatch.try({
                do{
                    let writer = EAN128Writer()
                    let result = try writer.encode(text, format: kBarcodeFormatCode128, width: Int32(ScreenSize.SCREEN_WIDTH * 2) , height: 200)
                    let cgImg = ZXImage(matrix: result).cgimage
                    let img = UIImage(cgImage: cgImg!)
                    self.imageView.image = img
                }
                catch{
                    print(error.localizedDescription)
                }
            }, catch: { (error) in
                self.imageView.image = nil
                print(error!.description)
//                self.lblError.text = error!.description
                self.lblError.text  = "Invalid content"
                self.lblError.isHidden = false
                self.lblError.sizeToFit()
            }, finallyBlock: nil)

        }
        else{
            let writer = ZXMultiFormatWriter.writer() as! ZXMultiFormatWriter
            SwiftTryCatch.try({
                do{
                    let width: Int32 = Int32(ScreenSize.SCREEN_WIDTH * 2)
                    var height: Int32 = 200
                    var zformat:ZXBarcodeFormat!
                    if self.selectedType == .CODE39{
                        zformat = kBarcodeFormatCode39
                    }
                    else if self.selectedType == .CODE128{
                        zformat = kBarcodeFormatCode128
                    }
                    else if self.selectedType == .JAN13{
                        zformat = kBarcodeFormatEan13
                    }
                    else if self.selectedType == .ITF{
                        zformat = kBarcodeFormatITF
                    }
                    else if self.selectedType == .QR{
                        zformat = kBarcodeFormatQRCode
                        height = Int32(ScreenSize.SCREEN_WIDTH * 2)
                    }
                    
                    let result = try writer.encode(text, format: zformat, width: width, height: height)
                    let cgImg = ZXImage(matrix: result).cgimage
                    let img = UIImage(cgImage: cgImg!)
                    self.imageView.image = img
                }
                catch{
                    
                }
            }, catch: { (error) in
                self.imageView.image = nil
                print(error!.description)
//                self.lblError.text = error!.description
                self.lblError.text = "Invalid content"
                self.lblError.isHidden = false
                self.lblError.sizeToFit()
            }, finallyBlock: nil)
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        self.generateBarCodeOrQR(format: selectedType)
    }

}
