//
//  Const.swift
//  AIC
//
//  Created by Than Htet Aung on 27/8/15.
//  Copyright (c) 2015 FlexTech. All rights reserved.
//

import Foundation
import UIKit

#if UAT
    let baseUrl = "http://54.255.173.234/api"; //  client UAT
#else
    let baseUrl = "http://54.255.173.234/api"; //client production
#endif


let CLIENT_ID = "b24569dea5384d759743976bccbf1161";
let CLIENT_SECRET = "T6M38%2#3m2qs";

let CACHE_KEY = "com.codigo.drypers";

let HTTP_REQUEST_STARTING_NOTIFICATION = "httpRequestStartNotification";
let HTTP_REQUEST_FINISHING_NOTIFICATION = "httpRequestFinishNotification";
let HTTP_UPLOAD_STARTING_NOTIFICATION = "httpUploadStartNotification";
let HTTP_UPLOAD_FINISHING_NOTIFICATION = "httpUploadFinishNotification";
let HTTP_UPLOAD_PROGRESS_NOTIFICATION = "httpUploadProgressNotification";

let SHOW_MENU_NOTIFICATION = "showNotification";
let IMAGE_LOADING_FINISHED_NOTIFICATION = "imageLoadingFinishNotification";
let HOME_NOTIFICATION = "homeNotification";
let SERVER_ERROR_NOTIFICATION = "serverErrorNotification";
let FILE_DOWNLOAD_FINISH_NOTIFICATION = "fileDownloadFinishNotification";
let UPDATE_FINISH_NOTIFICATION = "updateFinishNotification";
let DASHBOARD_BANNER_CHANGE_NOTIFICATION = "dashboardBannerChangeNotification";
let VERIFICATION_SUCCESS_NOTIFICATION = "verifciationSuccessNotification";
let VERIFICATION_FAIL_NOTIFICATION = "verifciationFailNotification";
let PAGE_CHANGE_NOTIFICATION = "pageChangeNotification";
let TAB_CHANGE_NOTIFICATION = "tabChangeNotification";
let SHOW_LOGIN_NOTIFICATION = "showLoginNotification";
let SHOW_POINT_NOTIFICATION = "showPointNotification";

let btnBoderColor = "#abd4ee";
let navColor = "#e1e558";
let navTitleColor = "#cb3822";
let primaryColor = "#1591e5";
let secondaryColor = "#F6A623";
let bgColor = "#e7e9eb";
let textFieldPaddingPoint:CGFloat = 10.0;
let fontName1 = "Oswald-Regular";
let fontName2 = "VarelaRound-Regular";
//let placeHolderColor = "#f7f3f3";
//let placeHolderColor = UIColor(hexString: "#f7f3f3", alpha: 0.7);


func buildUrl(url:String) -> String{
    return "\(baseUrl)/\(url)";
}

func addPathParam(url:String, pathParam:Int) -> String{
    return "\(url)/\(pathParam)";
}

func getDefaultBackgroundImage() -> UIImage{
    let url = getDocumentsDirectory().appendingPathComponent("default.png")
    
    if FileManager.default.fileExists(atPath: url.path){
        let data = try? Data(contentsOf: url)
        var img = UIImage(data: data! as Data)
        img = UIImage(cgImage: (img?.cgImage)!, scale: UIScreen.main.scale, orientation: .up)
        return img!
    }
    else{
        return UIImage(named:"bg")!
    }
}

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

func getDocumentFolder() -> String{
    return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
}

func makeDocumentPath(filename : String) -> String{
    let documentFolder = getDocumentFolder();
    return "\(documentFolder)/\(filename)";
}

func getAppDelegate() -> AppDelegate{
    return UIApplication.shared.delegate as! AppDelegate;
}

func postGlobalNotitication(name: String, param : AnyObject?) {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: name), object: param);
}

func addObserverForGlobalNotification(name : String, target : AnyObject, action : Selector, obj : AnyObject?) {
    NotificationCenter.default.addObserver(target, selector: action, name: NSNotification.Name(rawValue: name), object: obj);
}

func removeObserverForGlobalNotification(name : String?, target : AnyObject, obj : AnyObject?) {
    if let notiName = name {
        NotificationCenter.default.removeObserver(target, name: NSNotification.Name(rawValue: notiName), object: obj);
    }
    else{
        NotificationCenter.default.removeObserver(target);
    }
}

struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.height;
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}

struct DeviceType{
    static let IS_IPHONE4 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 480;
    static let IS_IPHONE5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0;
    static let IS_IPHONE6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0;
    static let IS_IPHONE6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0;
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad;
    
}
