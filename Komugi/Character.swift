//
//  Character.swift
//  Komugi
//
//  Created by Than Htet Aung on 31/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation

extension Character {
    var asciiValue: Int {
        get {
            let s = String(self).unicodeScalars
            return Int(s[s.startIndex].value)
        }
    }
    var isDigit: Bool {
        get {
            let s = String(self).unicodeScalars
            return CharacterSet.decimalDigits.contains(s.first!)
        }
    }
    
}
