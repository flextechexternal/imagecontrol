//
//  CheckDigit.swift
//  Komugi
//
//  Created by Than Htet Aung on 31/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation

class CheckDigit: NSObject {
    static let CDNone:Int8 = 0
    static let CD31:Int8 = 1
    static let CD11:Int8 = 2
    
    class func calcCheckdigit(msg:String, start:Int8, end:Int8, type:Int8)-> Character{
        switch (type) {
        case CD31 :    return calcCheckdigit(oddMult:3, evenMult:1, msg:msg, start:start, end:end)
        case CD11 :    return calcCheckdigit(oddMult:1, evenMult:1, msg:msg, start:start, end:end)
            default : return "0";
        }
    }
    
    class func calcCheckdigit31(msg:String, start:Int8, end:Int8)->Character{
        return calcCheckdigit(oddMult:3, evenMult:1, msg:msg, start:start, end:end)
    }
    
    class func calcCheckdigit11(msg:String, start:Int8, end:Int8)->Character {
        return calcCheckdigit(oddMult:1, evenMult:1, msg:msg, start:start, end:end)
    }
    
    class func calcCheckdigit(oddMult:Int8, evenMult:Int8, msg:String, start:Int8, end:Int8)->Character{
        var oddSum = 0
        var evenSum = 0
        var even = false
        
        for i in stride(from: (end - 1), through: start, by: -1){
            if even{
                evenSum += Int(String(msg.charAt(index:Int(i))))!
            } else {
                oddSum += Int(String(msg.charAt(index:Int(i))))!
            }
            even = !even;
        }
        
        var check = 10 - ((Int(evenMult) * evenSum + Int(oddMult) * oddSum) % 10);
        if check >= 10 {
            check = 0;
        }
        return String(check).characters.first!
    }
}
