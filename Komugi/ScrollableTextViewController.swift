//
//  ScrollableTextViewController.swift
//  Komugi
//
//  Created by Flex Tech on 3/20/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import UIKit

class ScrollableTextViewController: UIViewController {
    
    @IBOutlet weak var scrollableLabel: ScrollableLabel!
    @IBOutlet weak var userInputTextView: UITextView!

    var scrollableLable : UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
