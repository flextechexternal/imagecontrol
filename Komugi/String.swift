//
//  String.swift
//  Komugi
//
//  Created by Than Htet Aung on 31/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation
extension String {
    func charAt(index: Int) -> Character {
        return [Character](characters)[index]
    }
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
    func indexOf(_ sub: String) -> Int? {
        var pos: Int?
        
        if let range = self.range(of: sub) {
            if !range.isEmpty {
                pos = self.characters.distance(from: self.startIndex, to: range.lowerBound)
            }
        }
        
        return pos
    }
    func indexOf(_ target: Character, startIndex: Int) -> Int {
        return indexOf(String(target), startIndex: startIndex)
    }
    
    func indexOf(_ target: String, startIndex: Int) -> Int {
        let start = index(self.startIndex, offsetBy: startIndex)
        let end = self.index(self.startIndex, offsetBy: self.length)
        let r = start..<end
        let range = self.range(of: target, options: NSString.CompareOptions.literal, range: r, locale: nil)
        if let range = range {
            return distance(from: self.startIndex, to: range.lowerBound)
        } else {
            return -1
        }
    }
    
    var length:Int {
        return self.characters.count
    }

    
    func lastIndexOf(target: String) -> Int? {
        let range = (self as NSString).range(of: target, options: NSString.CompareOptions.backwards)
        guard range.toRange() != nil else {
            return nil
        }
        return self.length - range.location - 1
        
    }
}
