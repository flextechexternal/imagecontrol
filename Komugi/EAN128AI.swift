//
//  EAN128AI.swift
//  Komugi
//
//  Created by Than Htet Aung on 31/3/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation

enum AIError : Error {
    case ConversionError(String)
}

class AIData: NSObject{
    fileprivate static var dft:EAN128AI!
    fileprivate static var aiTable:NSMutableArray!
    
    private static var isLoaded = {
        () -> Bool in
        AIData.dft = try! EAN128AI.parseSpecPrivate(ai: "xx", spec: "an1-48")
        AIData.aiTable = NSMutableArray(array: [dft, dft, dft, dft, dft, dft, dft, dft, dft, dft])
        for i in 0...9{
            EAN128AI.initFixedLen(aiName: "23\(i)", aiLen: Int8((1 + 9 * i)))
        }
        for i in stride(from: (EAN128AI.fixedLenValueTable.count - 1), through: 0, by: -1){
            EAN128AI.initFixedLen(aiName: EAN128AI.fixedLenTable[i], aiLen: (EAN128AI.fixedLenValueTable[i] - 2))
        }
        return true
    }()
    
    override init() {
        //static loading
        _ = AIData.staticLoading
    }
    
    private static let staticLoading = isLoaded
}

class EAN128AI: NSObject {

    static let AIs:[String:String]=[
    "00":"n17+cd",
    "01":"n13+cd",
    "02":"n13+cd",
    "10":"an1-20",
    "11":"d6",
    "13":"d6",
    "15":"d6",
    "17":"d6",
    "20":"n2",
    "21":"an-20",
    "22":"an-29",
    "23x":"e",
    "230":"n1",
    "231":"n10",
    "232":"n19",
    "240":"an-30",
    "241":"an-30",
    "250":"an-30",
    "251":"an-30",
    "252":"n2",
    "30":"n-8",
    "310x":"n6",
    "311x":"n6",
    "312x":"n6",
    "313x":"n6",
    "314x":"n6",
    "315x":"n6",
    "316x":"n6",
    "32xx":"n6",
    "330x":"n6",
    "331x":"n6",
    "332x":"n6",
    "333x":"n6",
    "334x":"n6",
    "335x":"n6",
    "336x":"n6",
    "337x":"n6",
    "34xx":"n6",
    "350x":"n6",
    "351x":"n6",
    "352x":"n6",
    "353x":"n6",
    "354x":"n6",
    "355x":"n6",
    "356x":"n6",
    "36xx":"n6",
    "37":"n-8",
    "390x":"n-15",
    "391x":"n4-18",
    "392x":"n-15",
    "393x":"n4-18",
    "400":"an-30",
    "401":"an-30",
    "402":"n17",
    "403":"an-30",
    "410":"n12+cd",
    "411":"n12+cd",
    "412":"n12+cd",
    "413":"n12+cd",
    "414":"n12+cd",
    "415":"n12+cd",
    "420":"an-9",
    "421":"n3+an1-9",
    "422":"n3",
    "8001":"n14",
    "8002":"an-20",
    "8003":"n13+cd+an1-16",
    "8004":"an-30",
    "8005":"n6",
    "8006":"n13+cd+n2+n2",
    "8007":"an-30",
    "8008":"n9-12",
    "8018":"n18",
    "8020":"an-25",
    "8100":"n6",
    "8101":"n10",
    "8102":"n2",
    "9x":"an-30"]
    
    static let CONSTLenMax:Int8 = 48 // Max according to EAN128 specification.
    static let TYPEAlphaNum:Int8 = 0
    static let TYPENum:Int8 = 1
    static let TYPEAlpha:Int8 = 2    //Unused at the moment, but mentioned by
    
    //the EAN128 specification.
    static let TYPENumDate:Int8 = 3
    static let TYPEError:Int8 = 4
    static let TYPECD:Int8 = 5; //Check digit
    fileprivate static let typeToString = ["an", "n", "a", "d", "e", "cd"]
    
    var id:String!
    var lenID:Int8!
    var lenMinAll:Int8!
    var lenMaxAll:Int8!
    var minLenAfterVariableLen:Int8!
    
    var lenMin:[Int8] = []
    var lenMax:[Int8] = []
    var type:[Int8] = []
    var checkDigitStart:[Int8] = []
    
    var fixed = false
    var canDoChecksumADD = false;
    
    fileprivate static var fixedLenTable:[String] = [
        "00", "01", "02", "03", "04",
        "11", "12", "13", "14", "15", "16", "17", "18", "19",
        "20",
        "31", "32", "33", "34", "35", "36",
        "41"]
    
    fileprivate static var fixedLenValueTable:[Int8] = [
        20, 16, 16, 16, 18,
        8, 8, 8, 8, 8, 8, 8, 8, 8,
        4,
        10, 10, 10, 10, 10, 10,
        16]
    
    fileprivate static var propertiesLoaded = false

    
    class func loadProperties()throws{
        guard !propertiesLoaded else{
            return
        }
        
        for (key,value) in AIs{
            var ai = try parseSpecPrivate(ai: key, spec: value)
            try setAI(aiName: key, ai: &ai)
        }
        propertiesLoaded = true
    }
    
    
    
    fileprivate init(id:String, lenID:Int8, type:[Int8], lenMin:[Int8], lenMax:[Int8], checkDigitStart:[Int8])throws{
        super.init()
        self.id = id
        self.lenID = lenID;
        self.type = type;
        self.lenMin = lenMin;
        self.lenMax = lenMax;
        self.checkDigitStart = checkDigitStart;
        lenMinAll = Int8(0)
        lenMaxAll = Int8(0)
        minLenAfterVariableLen = Int8(0)
        var idxVarLen = type.count;
        var idxFirstChecksum = -1;
        //        canDoChecksumADD = true;
        for i in 0..<type.count {
            lenMinAll = lenMinAll + lenMin[i]
            lenMaxAll = lenMaxAll + lenMax[i]
            if i > idxVarLen{
                minLenAfterVariableLen = minLenAfterVariableLen + lenMin[i]
            }
            if lenMin[i] != lenMax[i] {
                if idxVarLen < type.count{
                    throw AIError.ConversionError("Only one Part with var len!") //TODO
                }
                idxVarLen = i
                
            }
            if idxFirstChecksum == -1 && type[i] == EAN128AI.TYPECD{
                idxFirstChecksum = i
            }
        }
        canDoChecksumADD = (idxFirstChecksum == type.count - 1 && lenMinAll == lenMaxAll);
        
    }
    
    fileprivate convenience init(id:String, spec:String, lenID:Int8, type:Int8, len:Int8)throws{
        try self.init(id: id, lenID: lenID, type: [type], lenMin: [len], lenMax: [len], checkDigitStart: [CheckDigit.CDNone])
        self.fixed = true
    }
    
    class func initFixedLen(aiName:String, aiLen:Int8) {
        let lenID = Int8(aiName.characters.count)
        do {
            var ai = try EAN128AI(id: aiName, spec: "an\(aiLen)", lenID: lenID, type: EAN128AI.TYPEAlphaNum, len: aiLen)
            try setAI(aiName: aiName, ai: &ai)
        } catch{
            print(error.localizedDescription)
        }
    }
    
    class func parseByte(val:String, dft:Int8, spec:String)throws->Int8 {
        let result = Int8(val)
        if result == nil{
            if dft == -1{
                throw AIError.ConversionError("Can't read field length from \"" + spec + "\"")
            }
            return dft
        }
        
        return result!
        
    }
    
    class func parseSpec(ai:String, spec:String)throws ->EAN128AI {
        var ret = try EAN128AI.parseSpecPrivate(ai:ai, spec: spec)
        _  = try checkAI(ai: &ret)
        return ret
    }
    
    class func parseSpecPrivate(ai:String, spec:String)throws->EAN128AI {
    
        let lenID:Int8 = Int8(ai.trimmingCharacters(in: .whitespacesAndNewlines).characters.count)
        let trimmedSpec = spec.trimmingCharacters(in: .whitespacesAndNewlines)
        let components = trimmedSpec.components(separatedBy: "+")
        var type:[Int8] = Array<Int8>(repeating: 0, count: components.count)
        var checkDigitStart:[Int8] = Array<Int8>(repeating: 0, count: components.count)
        var lenMin:[Int8] = Array<Int8>(repeating: 0, count: components.count)
        var lenMax:[Int8] = Array<Int8>(repeating: 0, count: components.count)
        
//        for _ in 0..<components.count{
//            type.append(0)
//            checkDigitStart.append(0)
//            lenMin.append(0)
//            lenMax.append(0)
//        }
        
        for (i,st) in components.enumerated(){
            try parseSpecPrivate(i: i, spec: st, type: &type, lenMin: &lenMin, lenMax: &lenMax, checkDigitStart: &checkDigitStart)
        }
        return try EAN128AI(id: ai, lenID: lenID, type: type, lenMin: lenMin, lenMax: lenMax, checkDigitStart: checkDigitStart);
//    } catch (IllegalArgumentException iae) {
//    throw iae;
//    } catch (Exception e) {
//    throw new IllegalArgumentException(
//    "Cannot Parse AI: \"" + ai + "\" spec: \"" + spec + "\" ");
//    }
    }
    
    
    class func parseSpecPrivate(i:Int, spec:String,
                          type:inout[Int8], lenMin:inout [Int8], lenMax:inout [Int8], checkDigitStart:inout [Int8]) throws{
//        var checkDigitStart = checkDigitStart
//        var lenMin = lenMin
//        var lenMax = lenMax
        var startLen = 0;
        checkDigitStart[i] = 1
        lenMin[i] = -1
        lenMax[i] = -1
        if spec.hasPrefix("an") {
            type[i] = EAN128AI.TYPEAlphaNum
            startLen = 2
        } else if (spec.hasPrefix("a")) {
            type[i] = EAN128AI.TYPEAlpha
            startLen = 1
        } else if (spec.hasPrefix("cd")) {
            type[i] = EAN128AI.TYPECD
            if spec.characters.count > 2 {
                checkDigitStart[i] = Int8(spec.substring(from: 2))!
            }
            lenMin[i] = 1
            lenMax[i] = 1
            return;
        } else if spec.hasPrefix("n") {
            type[i] = EAN128AI.TYPENum
            startLen = 1
        } else if spec.hasPrefix("d") {
            type[i] = EAN128AI.TYPENumDate;
            lenMin[i] = 6
            lenMax[i] = 6
            startLen = 1;
        } else if spec.hasPrefix("e") {
            type[i] = EAN128AI.TYPEError
            lenMin[i] = 0
            lenMax[i] = 0
            return;
        } else {
            throw AIError.ConversionError("Unknown type!")
        }
        
        let hyphenIdx = spec.indexOf("-", startIndex: startLen)
        if (hyphenIdx < 0) {
            lenMin[i] = try parseByte(val: spec.substring(from:Int(startLen)), dft: lenMin[i], spec: spec)
            lenMax[i] = lenMin[i]
        } else if (hyphenIdx == startLen) {
            lenMin[i] = 1;
            lenMax[i] = try parseByte(val: spec.substring(from:startLen + 1), dft: lenMax[i], spec: spec)
        } else { // hyphenIdx > startLen
            let start = spec.index(spec.startIndex, offsetBy: startLen)
            let end = spec.index(spec.startIndex, offsetBy: hyphenIdx)
            
            lenMin[i] = try parseByte(val: spec.substring(with: start..<end), dft: lenMin[i], spec: spec)
            lenMax[i] = try parseByte(val: spec.substring(from:hyphenIdx + 1), dft: lenMax[i], spec: spec)
        }
    
        if type[i] == EAN128AI.TYPENumDate {
            if (lenMin[i] != 6 || lenMax[i] != 6) {
                throw AIError.ConversionError("Date field ( \(spec) ) must have length 6!")
            }
        }
    }
    
    fileprivate class func setAIHere(ai:inout EAN128AI, aitParent: inout NSMutableArray)throws {
        for idx in 0...9 {
            try setAIHere(aiNew: &ai, aitParent: &aitParent, idx: idx)
        }
    }
    fileprivate class func setAIHere(aiNew:inout EAN128AI, aitParent:inout NSMutableArray, idx:Int)throws{
        let tmp:Any = aitParent[idx]
        if tmp is EAN128AI {
            var aiOld = tmp as! EAN128AI
            if aiNew.type[0] == TYPEError {
                aiOld.type[0] = TYPEError
            } else {
                try checkFixed(aiNew: &aiNew, aiOld: &aiOld)
                aitParent[idx] = aiNew;
            }
        } else { //tmp instanceof Object[]
            var arr = tmp as! NSMutableArray
            try setAIHere(ai: &aiNew, aitParent: &arr)
        }
    }
    
    fileprivate class func checkFixed(aiNew: inout EAN128AI, aiOld: inout EAN128AI) throws{
        if aiOld.fixed && !aiNew.fixed {
            if aiNew.lenMaxAll != aiNew.lenMinAll
                    || aiNew.lenID + aiNew.lenMinAll != aiOld.lenID + aiOld.lenMinAll {
                throw AIError.ConversionError("AI \"\(aiNew.toString())\" must have fixed len: \(aiOld.lenID!) + \(aiOld.lenMinAll!)")
            }
            aiNew.fixed = true
        }
    }
    
    
    class func setAI(aiName:String, ai: inout EAN128AI)throws{
        var aitParent = AIData.aiTable
        var aiLastRelevantIdx = aiName.characters.count - 1
        while aiLastRelevantIdx >= 0
        && !aiName.charAt(index: aiLastRelevantIdx).isDigit {
            aiLastRelevantIdx = aiLastRelevantIdx - 1
        }
        var tmp:Any!
        for i in 0...aiLastRelevantIdx {
            
            let idx = aiName.charAt(index: i).asciiValue - "0".charAt(index: 0).asciiValue;
            if (i == aiLastRelevantIdx) {
                try EAN128AI.setAIHere(aiNew: &ai, aitParent: &aitParent!, idx: idx)
            } else {
                tmp = aitParent![idx];
                if tmp is EAN128AI {
                    tmp = NSMutableArray(array: [tmp as Any, tmp as Any, tmp as Any, tmp as Any, tmp as Any, tmp as Any, tmp as Any, tmp as Any, tmp as Any, tmp as Any]) as Any
                    aitParent![idx] = tmp
                }
                aitParent = (tmp as! NSMutableArray)
            }
        }
    }
    
//    class func checkFixed(aiNew:EAN128AI, aiOld:EAN128AI) throws{
//        if aiOld.fixed && !aiNew.fixed{
//            if aiNew.lenMaxAll != aiNew.lenMinAll
//                || aiNew.lenID + aiNew.lenMinAll != aiOld.lenID + aiOld.lenMinAll {
//                throw AIError.ConversionError("AI \"\(aiNew.toString())\" must have fixed len: \(aiOld.lenID) + \(aiOld.lenMinAll)")
//            }
//            aiNew.fixed = true
//        }
//    }
    
    class func checkAI(ai:inout EAN128AI)throws->Bool {
        var aiCompare:EAN128AI = try getAIPrivate(msg:"\(ai.id!)0000", msgStart:0)
        try checkFixed(aiNew: &ai, aiOld: &aiCompare)
        return true;
    }
    
    class func getAI(msg:String, msgStart:Int)throws->EAN128AI {
        try loadProperties()
        return try getAIPrivate(msg: msg, msgStart: msgStart)
    }
    
    class func getAIPrivate(msg:String, msgStart:Int)throws->EAN128AI {
        var ret = AIData.dft
        var o:AnyObject! = AIData.aiTable as AnyObject;
        var c:Int
        for i in 0..<(msg.characters.count - msgStart) {
            c = try getIDChar(msg:msg, msgStart:(msgStart + i)).asciiValue - "0".characters.first!.asciiValue
            o = (o as! [AnyObject])[c]
            if o == nil {
                return AIData.dft
            }
            if o is EAN128AI{
                ret = (o as! EAN128AI)
                break
            }
        }
        return ret!
    }
    
    class func getIDChar(msg:String, msgStart:Int)throws->Character{
        var ret:Character
        if (msg.characters.count - 1) < msgStart || msgStart < 0{
            throw AIError.ConversionError("Unable to read AI: Message too short!")
        }
        ret = msg.charAt(index: msgStart)
        
        if !ret.isDigit {
            throw AIError.ConversionError("Unable to read AI: Characters must be numerical!")
        }
        return ret;
    }

    class func isCheckDigitType(type:Int8)->Bool{
        return type == EAN128AI.TYPECD
    }
    
    func isCheckDigit(idx:Int8)->Bool {
        return EAN128AI.isCheckDigitType(type: type[Int(idx)])
    }
    
    class func getType(type:Int8)->String {
        var ret = "?"
        if type >= 0 && Int(type) < EAN128AI.typeToString.count{
            ret = EAN128AI.typeToString[Int(type)]
        }
        return ret;
    }
    
    func toString()->String {
        var ret = ""
        ret.append("(")
        ret.append(id)
        ret.append(")")
        for i in 0..<lenMin.count {
            if i != 0 {
                ret.append("+")
            }
            ret.append(EAN128AI.getType(type: type[i]))
        //            if (checkDigit[i] == CheckDigit.CD11)
        //                ret.append("w1");
            if type[i] < EAN128AI.TYPEError {
                ret.append("\(lenMin[i])")
                if lenMin[i] != lenMax[i] {
                    ret.append("-")
                    ret.append("\(lenMax[i])")
                }
            }
        }
        if fixed{
            ret.append(" (fixed)")
        }
        return ret
    }
    
}
