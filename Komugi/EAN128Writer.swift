//
//  EAN128Writer.swift
//  Komugi
//
//  Created by Than Htet Aung on 1/4/17.
//  Copyright © 2017 zarniaung. All rights reserved.
//

import Foundation
import ZXingObjC



class EAN128Writer:  ZXOneDimensionalCodeWriter{
    fileprivate static let MAX_LENGTH:Int8 = 48 // Max according to EAN128 specification.
    
    fileprivate static let TYPENumTestCheckDigit:Int8 = 4
    private static let  TYPENumReplaceCheckDigit:Int8 = 5
    private static let TYPENumAddCheckDigit = 6
    
    /** The function 1 command. ASCII: 0xF1 */
    public static let FNC_1:Character = "\u{F1}"
    /** The function 2 command. ASCII: 0xF2 */
    public static let FNC_2:Character = "\u{F2}"
    /** The function 3 command. ASCII: 0xF3 */
    public static let FNC_3:Character = "\u{F3}"
    /** The function 4 command. ASCII: 0xF4 */
    public static let FNC_4:Character = "\u{F4}"
    
    private var ais:[EAN128AI]!
    //GroupSeperator not Code128LogicImpl.FNC_1;
    private let groupSeparator:Character = "\u{1D}"
    
    private let checkDigitMarker:Character = "\u{F0}"
    private var omitBrackets:Bool = false
    
    private var msgCache:String!
    private var code128Msg:String!
    private var humanReadableMsg:String!
    private var encodedMsg:[Int] = []
    
    private var checksumADD:Bool = true
    private var checksumCHECK:Bool = true

    
    
    let writer = ZXCode128Writer()
    override func encode(_ contents: String!)-> ZXBoolArray! {
        if contents.length > Int(EAN128Writer.MAX_LENGTH){
            NSException(name: .invalidArgumentException, reason: "Maximum length of EAN128 is 48", userInfo: nil).raise()
        }
        code128Msg = ""
        humanReadableMsg = ""
        code128Msg.append(EAN128Writer.FNC_1)
        do{
            try addAIs(msg: contents)
        }
        catch AIError.ConversionError(let msg){
            NSException(name: .invalidArgumentException, reason: msg, userInfo: nil).raise()
        }catch{
            NSException(name: .invalidArgumentException, reason: error.localizedDescription, userInfo: nil).raise()
        }
        return writer.encode(code128Msg)
    }
    
    func addAIs(msg:String)throws{
        //static loading
        _ = AIData()
        
        var offset = 0
        var i = 0
        var ai:EAN128AI?
        while offset < msg.length {
            if ais == nil {
                ai = nil
            } else {
                i += 1
                guard (i >= 0 && i < self.ais.count) else {
                    throw AIError.ConversionError("Message has more AIs than template (template has \(ais.count))")
                }
                ai = ais[i]
                
            }
            offset = try addAI(msg: msg, offset: offset, ai: &ai)
        }
    }
    
    private func findGroupSeparator(msg:String, start:Int)->Int {
        let retGS = msg.indexOf(groupSeparator, startIndex: start)
        if groupSeparator == EAN128Writer.FNC_1 {
            return retGS
        }
        let retF = msg.indexOf(EAN128Writer.FNC_1, startIndex: start)
        if retGS <= 0 {
            return retF
        }
        if retF <= 0 {
            return retGS
        }
        return min(retGS, retF)
    }
    
    func addAI(msg:String?, offset:Int, ai:inout EAN128AI?)throws ->Int{
        if msg == nil {
            throw getException("Message is empty!")
        }
        do {
            if ai == nil {
                ai = try EAN128AI.getAI(msg: msg!, msgStart: offset)
            }
        } catch AIError.ConversionError(let desc){
            throw getException(desc)
        }catch{
            throw getException(error.localizedDescription)
        }
        let lenID = Int(ai!.lenID)
    //        byte type = ai.type[0];
        let lenMin = Int(ai!.lenMinAll)
        let lenMax = Int(ai!.lenMaxAll)
    
        if !omitBrackets {
            humanReadableMsg.append("(")
        }
        if offset + lenID >= msg!.length{
            throw getException("Invalid content")
        }
        let temp = msg!.substring(with:offset..<(offset + lenID))
        humanReadableMsg.append(temp)
        code128Msg.append(temp) //BUG fixed 15.08!!
        if !omitBrackets {
            humanReadableMsg.append(")")
        }
        var doChecksumADD = false
        var startA:[Int] = Array<Int>(repeating: 0, count: ai!.type.count + 1)
//        for _ in 0...ai!.type.count + 1{
//            startA.append(0)
//        }
        startA[0] = offset;
        
    //        start[1] = offset + lenID;
        var newOffset = findGroupSeparator(msg: msg!, start: offset)
        if newOffset < 0 {
            newOffset = msg!.length
        }
        if newOffset < offset + lenID + lenMin {
            if checksumADD && ai!.canDoChecksumADD
                && newOffset == offset + lenID + lenMin - 1 {
                doChecksumADD = true;
            } else if (ai!.fixed || lenMin == lenMax) && newOffset < msg!.length {
                throw getException("FNC1 not allowed in fixed length field: \" \(msg!.substring(with:offset + lenID..<min(msg!.length, offset + lenID + lenMax))) \"!");
            } else {
                throw getException("Field \" \(msg!.substring(with:offset + lenID..<newOffset)) \" too short! Length should be \(lenMin) at least!")
            }
    
        }
        if newOffset > offset + lenID + lenMax {
            if ai!.fixed || lenMin == lenMax {
                newOffset = offset + lenID + lenMax
            } else {
                throw getException("Variable length field \" \(msg!.substring(with:offset + lenID..<newOffset)) \" too long! Length should be \(lenMax) at the most!")
            }
        }
        var start = offset + lenID
        var end = 0
        for i in 0..<ai!.type.count{
            startA[i + 1] = start
            if ai!.lenMin[i] == ai!.lenMax[i] {
                end = start + Int(ai!.lenMin[i])
            } else {
                end = newOffset - Int(ai!.minLenAfterVariableLen)
            }
    //            if (ai.checkDigit[i] != CheckDigit.CDNone && !doChecksumADD && checksumCHECK){
    //                char cd1 = CheckDigit.calcCheckdigit(msg, cdStart, start, ai.checkDigit[i]);
    //                char cd2 = msg.charAt(start);
    //                if (cd1 != cd2)
    //                    throw getException("Checkdigit is wrong! Correct is " + cd1
    //                        + " but I found " + cd2 + "!", msg.substring(cdStart, start));
    //                humanReadableMsg.append(cd1);
    //                code128Msg.append(cd1);
    //            } else if (ai.checkDigit[i] == CheckDigit.CDNone || !doChecksumADD) {
            if doChecksumADD && i == ai!.type.count - 1 { //ai.checkDigit[i] != CheckDigit.CDNone) {
                let temp = startA[Int(ai!.checkDigitStart[i])]
                let c = CheckDigit.calcCheckdigit(msg: msg!, start: Int8(temp), end: Int8(start), type: CheckDigit.CD31)
                humanReadableMsg.append(c)
                code128Msg.append(c)
                if (newOffset < msg!.length
                    && isGroupSeparator(ch:msg!.charAt(index:newOffset))) {
                    newOffset += 1
                }
            } else {
                try checkType(ai: ai!, idx:Int8(i), msg: msg!, start: start, end: end, cdStart: startA[Int(ai!.checkDigitStart[i])])
    //                humanReadableMsg.append(msg.substring(startI, end));
    //                code128Msg.append(msg.substring(startI, end));
            }
            start = end
        }
    //        if (doChecksumADD) {
    //            char c = CheckDigit.calcCheckdigit(msg, cdStart, newOffset, ai.checkDigit[ai.checkDigit.length-1]);
    //            humanReadableMsg.append(c);
    //            code128Msg.append(c);
    //            if (newOffset < msg.length() && msg.charAt(newOffset) == groupSeparator) {
    //                newOffset++;
    //            }
    //        }
        if newOffset < msg!.length
            && isGroupSeparator(ch:msg!.charAt(index:newOffset)) {
    //TODO Needed for 8001?...
            newOffset += 1
    //            code128Msg.append(Code128LogicImpl.FNC_1);
        }
        if !ai!.fixed && newOffset < msg!.length {
            code128Msg.append(EAN128Writer.FNC_1);
        }
        return newOffset
    //TODO check that every id appears only once in the barcode
    //TODO check that encodedMsg is never larger than 35
    //TODO check that message never larger than 48 (does this include all groupSeparator?)
    //TODO show only 13 digits of the EAN in human readable part (if the first digit is '0')
    }
    
    private func isGroupSeparator(ch:Character)->Bool{
        return (ch == groupSeparator || ch == EAN128Writer.FNC_1)
    }
    
    private func checkType(ai:EAN128AI, idx:Int8, msg:String, start:Int, end:Int, cdStart:Int)throws{
        let type = ai.type[Int(idx)]
        if (type == EAN128AI.TYPEError) {
            throw getException("This AI is not allowed by configuration! (\(ai.toString()))")
    
        } else if (type == EAN128AI.TYPEAlpha) {
            for i in stride(from: (end - 1), through: start, by: -1){
                if msg.charAt(index:i).asciiValue > 128 || msg.charAt(index:i).isDigit {
                    throw getException("Character \' \(msg.charAt(index:i))' must be a valid ASCII byte but not number!", msg.substring(with: start..<i))
                }
            }
        } else if type == EAN128AI.TYPEAlphaNum{
            for i in stride(from: (end - 1), through: start, by: -1){
                if msg.charAt(index:i).asciiValue > 128 {
                    throw getException("Character '\(msg.charAt(index:i))' must be a valid ASCII byte!",
                        msg.substring(with:start..<i))
                }
            }
        } else {
            if ai.isCheckDigit(idx: idx) && checksumCHECK{
                let cd1 = CheckDigit.calcCheckdigit(msg: msg, start: Int8(cdStart), end: Int8(start), type: CheckDigit.CD31)
                var cd2 = msg.charAt(index:start)
                if cd2 == checkDigitMarker{
                    cd2 = cd1
                }
                if cd1 != cd2 {
                    throw getException("Checkdigit is wrong! Correct is \(String(cd1)) but I foud \(String(cd2)) !")
                }
                humanReadableMsg.append(cd1);
                code128Msg.append(cd1);
                return;
            }
            for i in stride(from: (end - 1), through: start, by: -1){
                if !msg.charAt(index:i).isDigit {
                    throw getException("Character ' \(String(msg.charAt(index:i)))' must be a Digit!",
                        msg.substring(with:start..<i))
                }
            }
            if type == EAN128AI.TYPENumDate{
                let cm1 = msg.charAt(index:start + 2)
                let cm2 = msg.charAt(index:start + 3)
                let cd1 = msg.charAt(index:start + 4)
                let cd2 = msg.charAt(index:start + 5)
                if (cm1 == Character("0") && cm2 == Character("0"))
                    || (cm1 == Character("1") && cm2.asciiValue > "2".characters.first!.asciiValue)
                    || (cm1.asciiValue > "1".characters.first!.asciiValue) {
                    throw getException("Illegal Month \" \(cm1) \(cm2) \"!",
                        msg.substring(with:start..<(start + 2)))
                }
                if (cd1 == Character("3") && cd2.asciiValue > "1".characters.first!.asciiValue) || cd1.asciiValue > "3".characters.first!.asciiValue {
                    throw getException("Illegal Day \" \(cd1) \(cd2) \"!",
                        msg.substring(with:start..<(start + 4)))
                }
            }
        }
        humanReadableMsg.append(msg.substring(with:start..<end))
        code128Msg.append(msg.substring(with:start..<end))
    }
    
    private func getIDChar(msg:String, offset:Int)throws->Character{
        if msg.length - 1 > offset{
            throw getException("Unable to read last ID: Message too short!")
        }
        let ret = msg.charAt(index: offset)
        if !ret.isDigit {
            throw getException("Unable to read last ID: Characters must be numerical!")
        }
        return ret
    }
    
    private func getException(_ text:String)->AIError{
        return getException(text, "")
    }
    
    private func getException(_ text:String,_ msgOk:String?)->AIError{
        var msgOk = msgOk
        var text = text
        if let _ = msgOk{
            msgOk = ""
        }
        if humanReadableMsg.length > 1 || msgOk!.length > 0{
            text = "\(text) Accepted start of Message: \" \(humanReadableMsg) \(msgOk!) \""
        }
        return AIError.ConversionError(text)
    }
    
}
